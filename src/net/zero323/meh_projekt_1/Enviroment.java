package net.zero323.meh_projekt_1;

import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Maciej Szymkiewicz
 * Klasa reprezentująca środowisko działania algorytmu
 *
 */
public class Enviroment {
	
	private static RandomGenerator rg;
	private int numberOfSpecimens;
	private int numberOfEliteSpecimens;
	private int numberOfSpecimensInTournament;
	private int envNumberOfGenes;
	private int envLengthOfGene;
	private long maxNumberOfIterations;
	private long maxNumberOfIterationsWithoutBetterFitness;
	private long iteration;
	private long iterationOfTheBestSolution;
	private long iterationsWithoutBetterFitness;
	private double theBestSolution;
	private Specimen fittestSpecimen;
	private double probabilityOfMutation;
	private double probabilityOfCrossing;
	private Specimen population[];
	private Specimen tournamentPopulation[];
	private Specimen populationAfterTournament[];
	private Specimen subpopulationOfEliteSpecimens[];
	private Specimen offspringPopulation[];

	/**
	 * Konstruktor klasy
	 * @param numberOfSpecimens - liczba osobników w populacji
	 * @param numberOfEliteSpecimens - liczba najlepszych osobników z populacji bazowej wybierana do sukcesji elitarnej
	 * @param numberOfSpecimensInTournament - liczba osobników w turnieju
	 * @param envNumberOfGenes - liczba genów (wymiar zadania)
	 * @param envLengthOfGene - długość pojedyńczego genu
	 * @param maxNumberOfIterations - maksymalna liczba iteracji
	 * @param maxNumberOfIterationsWithoutBetterFitness - maksymalna liczba iteracji bez uzyskania lepszego rozwiązania po których następuje zatrzymanie algorytmu
	 * @param probabilityOfMutation - prawdopodobieństwo mutacji bitu genu.
	 * @param probabilityOfCrossing - pradwpodobieństwo zamiany bitów w trakcie krzyżowania
	 */
	public Enviroment(
			final int numberOfSpecimens,
			final int numberOfEliteSpecimens,
			final int numberOfSpecimensInTournament,
			final int envNumberOfGenes,
			final int envLengthOfGene,
			final long maxNumberOfIterations,
			final long maxNumberOfIterationsWithoutBetterFitness,
			final double probabilityOfMutation,
			final double probabilityOfCrossing) {
		this.numberOfSpecimens = numberOfSpecimens;
		this.numberOfEliteSpecimens = numberOfEliteSpecimens;
		if (numberOfSpecimensInTournament < 1) {
			throw new IllegalArgumentException("numberOfSpecimensInTournament should be >= 1"); 
		}
		else {
			this.numberOfSpecimensInTournament = numberOfSpecimensInTournament;
		}
		this.envNumberOfGenes = envNumberOfGenes;
		this.envLengthOfGene = envLengthOfGene;
		this.maxNumberOfIterations = maxNumberOfIterations;
		this.maxNumberOfIterationsWithoutBetterFitness = maxNumberOfIterationsWithoutBetterFitness;
		this.iteration = 0;
		this.iterationOfTheBestSolution = 0;
		this.iterationsWithoutBetterFitness = 0;
		this.theBestSolution = 99999999;
		this.fittestSpecimen = null;
		this.probabilityOfMutation = probabilityOfMutation;
		this.probabilityOfCrossing = probabilityOfCrossing;
		this.population = new Specimen[this.numberOfSpecimens];
		this.subpopulationOfEliteSpecimens = new Specimen[numberOfEliteSpecimens];
		this.tournamentPopulation = new Specimen[numberOfSpecimensInTournament];
		this.populationAfterTournament = new Specimen[numberOfSpecimens];
		this.offspringPopulation = new Specimen[numberOfSpecimens];
	}
	
	/**
	 * Tworzenie losowej populacji początkowej
	 */
	public void populate() {
		for (int i = 0; i < this.numberOfSpecimens; i++){
			this.population[i] = new Specimen(this.envNumberOfGenes, this.envLengthOfGene);
		}
	}
	
	/**
	 * Realizacja turnieju przy populacji tymczasowej równej populacji wyjściowej, losowanie ze zwracaniem
	 */

	public void tournament() {
		int tempIndex = 0;
		ArrayList<Integer> listOfIndexes = new ArrayList<Integer>(this.numberOfSpecimens);
		for(int i=0; i<this.numberOfSpecimens; i++) {
			for(int indexOfLOI=0; indexOfLOI<this.numberOfSpecimens; indexOfLOI++) {
				listOfIndexes.add(indexOfLOI);
			}
			for(int indexOfTP=0; indexOfTP<this.numberOfSpecimensInTournament; indexOfTP++) {
				tempIndex = getRandomGenerator().nextInt(listOfIndexes.size());
				this.tournamentPopulation[indexOfTP] = this.population[listOfIndexes.get(tempIndex)];
				listOfIndexes.remove(tempIndex);
			}
			Arrays.sort(tournamentPopulation, new SpecimenComparator());
			this.populationAfterTournament[i] = this.tournamentPopulation[0];
		}
	}
	
	/**
	 * Wybór najlepszych osobników (do sukcesji elitarnej) 
	 */
	public void extractingEliteSpecimens() {
		Arrays.sort(population, new SpecimenComparator());
		for (int i = 0; i < this.numberOfEliteSpecimens; i++) {
			this.subpopulationOfEliteSpecimens[i] = this.population[i].incrementAge();
		}
	}
	
	/**
	 * Krzyżowanie osobników w populacji
	 */
	public void crossbreeding() {
		int iPAT;
		int i1;
		int i2;
		ArrayList<Integer> indexesOfPAT = new ArrayList<Integer>(this.populationAfterTournament.length);
		
		for (int i = 0; i < this.populationAfterTournament.length; i++) {
			indexesOfPAT.add(i);
		}
		
		while(indexesOfPAT.size() > 1) {
			iPAT = getRandomGenerator().nextInt(indexesOfPAT.size());
			i1 = indexesOfPAT.get(iPAT);
			indexesOfPAT.remove(iPAT);
			iPAT = getRandomGenerator().nextInt(indexesOfPAT.size());
			i2 = indexesOfPAT.get(iPAT);
			indexesOfPAT.remove(iPAT);
			
			offspringPopulation[i1] = populationAfterTournament[i1]
				.crossbreed(populationAfterTournament[i2],
				new Chromosome(envNumberOfGenes, envLengthOfGene, true, this.probabilityOfCrossing));

			offspringPopulation[i2] = populationAfterTournament[i2]
				.crossbreed(populationAfterTournament[i1],
				new Chromosome(envNumberOfGenes, envLengthOfGene, true, this.probabilityOfCrossing));
		}
	}
	
	
	/**
	 * Mutacje osobników potomnych 
	 */
	public void mutating() {
		for (int i = 0; i < offspringPopulation.length; i++) {
			offspringPopulation[i] = offspringPopulation[i].mutate(probabilityOfMutation);
		}
	}
	
	/**
	 * Sukcesja elitarna 
	 */
	public void succession() {
		Specimen outputPopulation[] = new Specimen[this.numberOfSpecimens + this.numberOfEliteSpecimens];
		int i = 0;
		for (Specimen specimen : this.offspringPopulation) {
			outputPopulation[i] = specimen;
			i++;		
		}
		for (Specimen specimen : this.subpopulationOfEliteSpecimens) {
			outputPopulation[i] = specimen;
			i++;		
		}
		Arrays.sort(outputPopulation, new SpecimenComparator());

		for (int j = 0; j < this.numberOfSpecimens; j++) {
			this.population[j] = outputPopulation[j];
		}
	}
	
	/**
	 * Ocena wyników iteracji 
	 */
	public void evaluate() {
		Arrays.sort(this.population, new SpecimenComparator());
		if(this.population[0].getFitness() < this.theBestSolution) {
			this.theBestSolution = this.population[0].getFitness();
			this.fittestSpecimen = population[0];
			this.iterationsWithoutBetterFitness = 0;
			this.iterationOfTheBestSolution = this.iteration;
		}
		else {
			this.iterationsWithoutBetterFitness +=1;
		}
		this.iteration++;
	}
	
	/**
	 * Funkcja pomocnicza agregująca kolejne etapy każdej iteracji
	 */
	public void doIteration() {
		this.extractingEliteSpecimens();
		this.tournament();
		this.crossbreeding();
		this.mutating();
		this.succession();
		this.evaluate();
	}
	
	
	/**
	 * Uruchomienie algorytmu
	 */
	public void startAlgorithm() {
		this.populate();
		for(int i = 0; i < this.maxNumberOfIterations; i++) {
			this.doIteration();
			if(this.iterationsWithoutBetterFitness >= this.maxNumberOfIterationsWithoutBetterFitness) {
				break;
			}
		}
	}
	
	/**
	 * Przywrócenie początkowego stanu środowiska 
	 */
	public void resetEnviroment() {
		rg = null;
		this.iteration = 0;
		this.fittestSpecimen = null;
		this.iterationsWithoutBetterFitness = 0;
		this.theBestSolution = 99999999;
		this.population = new Specimen[this.numberOfSpecimens];
		this.subpopulationOfEliteSpecimens = new Specimen[this.numberOfEliteSpecimens];
		this.tournamentPopulation = new Specimen[this.numberOfSpecimensInTournament];
		this.populationAfterTournament = new Specimen[this.numberOfSpecimens];
		this.offspringPopulation = new Specimen[this.numberOfSpecimens];
	}
	
	/**
	 * Funkcja pomocnicza do drukowania wyników działania algorytmu
	 * @return
	 */
	public String header() {
		String header = "numberOfSpecimens; numberOfEliteSpecimens; numberOfSpecimensInTournament; " + 
						"probabilityOfMutation; probabilityOfCrossing; theBestSolution; iterationOfTheBestSolution";
		return header;
	}
	
	/**
	 * Funkcja pomocnicza do drukowania wyników działania algorytmu
	 * @return
	 */
	public String summary(final Boolean printPopulation) {
		String line = "";
		line += this.numberOfSpecimens + "; ";
		line += this.numberOfEliteSpecimens + "; ";
		line += this.numberOfSpecimensInTournament + "; ";
		line += this.probabilityOfMutation + "; ";
		line += this.probabilityOfCrossing + "; ";
		line += this.theBestSolution + "; ";
		line += this.iterationOfTheBestSolution;
		if(printPopulation) {
			return line + "\n\n" + this.populationToString();
			}
		else {
			return line;
		}
	}
	
	/**
	 * Funkcja pomocnicza do drukowania struktury populacji
	 * @return
	 */
	public String populationToString() {
		String representation = "";
		for (int i = 0; i < this.envNumberOfGenes; i++) {
			representation += "x" + i + "; ";
		}
		for (int i = 0; i < this.envNumberOfGenes; i++) {
			representation += "b" + i + "; ";
		}
		representation += "fitness\n";
		
		for (Specimen specimen : population) {
			representation += specimen.fenotypeToString() + "; " + specimen.genotypeToString() + "; " +  specimen.getFitness() + "\n";
		}
		return representation;
	}
	
	/**
	 * Funkcja pomocnicza
	 * @return
	 */
	public void testEnviromentSettings(final int iterations, final Boolean printPopulation) {
		System.out.println(this.header());
		for (int i = 0; i < iterations; i++) {
			this.startAlgorithm();
			System.out.println(this.summary(printPopulation));
			this.resetEnviroment();
		}
		
	}
	
	/**
	 * Inicjowanie generatora liczb pseudolosowych
	 */
	public static RandomGenerator getRandomGenerator() {
		if (rg == null) {
			rg = new Well44497b(new Date().getTime());
		}
		return rg;
	}
	
	
	public int getEnvNumberOfGenes() {
		return envNumberOfGenes;
	}

	public int getEnvLengthOfGene() {
		return envLengthOfGene;
	}
	
}
