package net.zero323.meh_projekt_1;

import java.util.Comparator;

/**
 * @author Maciej Szymkiewicz
 * Comaparator dla osobników 
 *
 */
public class SpecimenComparator implements Comparator<Specimen>{

	@Override
	public int compare(Specimen o1, Specimen o2) {
		if (o1.getFitness() < o2.getFitness())	
			return -1;
		else if (o1.getFitness() > o2.getFitness())
			return 1;
		else
			return 0;
	}

}
