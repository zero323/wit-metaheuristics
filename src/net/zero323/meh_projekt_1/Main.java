package net.zero323.meh_projekt_1;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Maciej Szymkiewicz
 * 
 */
public class Main {
	private static int numberOfSpecimens;
	private static int numberOfEliteSpecimens;
	private static int numberOfSpecimensInTournament;
	private static int envNumberOfGenes;
	private static int envLengthOfGene;
	private static int maxNumberOfIterations;
	private static int maxNumberOfIterationsWithoutBetterFitness;
	private static double probabilityOfMutation;
	private static int numberOfTestIterations;
	private static double probabilityOfCrossing;
	
	public static void main(String[] args) {
		run(args);
		defaultSettings();
		Enviroment e;
		envNumberOfGenes = 3;
		maxNumberOfIterations = 1000;
		e = getEnviroment();
		e.testEnviromentSettings(10, false);
		
	
	}
	
	public static void run (String[] args) {
		if(args.length > 0) {
			if (args[0].equals("test")) {
				if (args.length == 2) {
					numberOfTestIterations = Integer.parseInt(args[1]);
				}
				defaultSettings();
				runTests(numberOfTestIterations);
			}
			
			else if (args[0].equals("properties")) {
				getProperties();
				Enviroment e = getEnviroment();
				if(args.length == 2) {
					if(args[1].equals("printPopulation")) {
						e.testEnviromentSettings(1, true);
					}
				}
				else {
					e.testEnviromentSettings(1, false);
				}
			}
			
			else if (args[0].equals("default")) {
				defaultSettings();
				Enviroment e = getEnviroment();
				if(args.length == 2) {
					if(args[1].equals("printPopulation")) {
						e.testEnviromentSettings(1, true);
					}
				}
				else {
					e.testEnviromentSettings(1, false);
				}
			}
		}
	}
	
	static public void defaultSettings(){
		numberOfSpecimens = 200;
		numberOfEliteSpecimens = (int)(numberOfSpecimens * 0.1);
		numberOfSpecimensInTournament = (int)(numberOfSpecimens * 0.1);
		envNumberOfGenes = 2;
		envLengthOfGene = 32;
		maxNumberOfIterations = 200000;
		maxNumberOfIterationsWithoutBetterFitness = 20000;
		probabilityOfMutation = 0.05;
		numberOfTestIterations = 25;
		probabilityOfCrossing = 0.5;
	}
	
	public static Enviroment getEnviroment() {
		return new Enviroment(
				numberOfSpecimens,
				numberOfEliteSpecimens,
				numberOfSpecimensInTournament,
				envNumberOfGenes,
				envLengthOfGene,
				maxNumberOfIterations,
				maxNumberOfIterationsWithoutBetterFitness,
				probabilityOfMutation,
				probabilityOfCrossing);
	}
	
	public static void getProperties() {
		Properties prop = new Properties();
		try {
	        FileInputStream inputStream = new FileInputStream("./meh.properties");
	        prop.load(inputStream);
	        numberOfSpecimens = Integer.parseInt(prop.getProperty("numberOfSpecimens"));
	        numberOfEliteSpecimens = Integer.parseInt(prop.getProperty("numberOfEliteSpecimens"));
	        numberOfSpecimensInTournament = Integer.parseInt(prop.getProperty("numberOfSpecimensInTournament"));
	        envNumberOfGenes = Integer.parseInt(prop.getProperty("envNumberOfGenes"));
	        envLengthOfGene = Integer.parseInt(prop.getProperty("envLengthOfGene"));
	        maxNumberOfIterations = Integer.parseInt(prop.getProperty("maxNumberOfIterations"));
	        maxNumberOfIterationsWithoutBetterFitness = Integer.parseInt(prop.getProperty("maxNumberOfIterationsWithoutBetterFitness"));
	        probabilityOfMutation = Double.parseDouble(prop.getProperty("probabilityOfMutation"));
	        probabilityOfCrossing = Double.parseDouble(prop.getProperty("probabilityOfCrossing"));
	        numberOfTestIterations = Integer.parseInt(prop.getProperty("numberOfTestIterations"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}   
		
	}
	
	public static void runTests(int numberOfIterations) {
		/* Test dla różnej wielkości populacji */
		System.out.println(
			"##############################################################\n" +
			"#        Test dla różnej wielkości populacji                 #\n" +
			"##############################################################\n\n");
		defaultSettings();
		int optNumberOfSpecimens[] = {50,100,200,500,1000};
		for (int i : optNumberOfSpecimens) {
			numberOfSpecimens = i;
			numberOfEliteSpecimens = (int)(numberOfSpecimens * 0.10);
			numberOfSpecimensInTournament = (int)(numberOfSpecimens * 0.10);

			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnej wielkości populacji elitarnej */ 
		System.out.println(
				"##############################################################\n" +
				"#       Test dla różnej wielkości populacji elitarnej        #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optPercentOfEliteSpecimens[] = {0.0, 0.10, 0.50, 0.75, 1.0};
		for (double d : optPercentOfEliteSpecimens) {
			numberOfEliteSpecimens = (int)(numberOfSpecimens * d);
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnej wielkości grupy turniejowej */
		System.out.println(
				"##############################################################\n" +
				"#       Test dla różnej wielkości grupy turniejowej          #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optPercentOfSpecimensInTournament[] = {0.005, 0.10, 0.50, 0.75, 1.0};
		for (double d : optPercentOfSpecimensInTournament) {
			numberOfSpecimensInTournament = (int)(numberOfSpecimens * d);
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnych wartości prawdopodobieństwa mutacji*/ 
		System.out.println(
				"##############################################################\n" +
				"#   Test dla różnych wartości prawdopodobieństwa mutacji     #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optProbabilityOfMutation[] = {0.0, 0.05, 0.1, 0.25, 0.50, 0.75, 0.9, 0.95, 1.00};
		for (double d : optProbabilityOfMutation) {
			probabilityOfMutation = d;
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnych wartości prawdopodobieństwa krzyżowania genów*/ 
		System.out.println(
				"##############################################################\n" +
				"#  Test dla różnych wartości prawdopodobieństw krzyżowania   #\n" +
				"#  genów                                                     #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optProbabilityOfCrossing[] = {0.0, 0.05, 0.1, 0.25, 0.50, 0.75, 0.9, 0.95};
		for (double d : optProbabilityOfCrossing) {
			probabilityOfCrossing = d;
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
	}

}
