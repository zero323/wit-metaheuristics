package net.zero323.meh_projekt_1;

import java.util.BitSet;
import org.apache.commons.math.random.RandomGenerator;

/**
 * @author Maciej Szymkiewicz
 * Klasa będąca reprezentacją genoptypu
 *
 */
public class Chromosome {
	/**
	 * Wartość pomocnicza dla wyliczania wartości fenotypu
	 */
	private static double domain = 30;
	/**
	 * Bitowa eprezentacja genotypu
	 */
	private BitSet genome;
	/**
	 * Liczba genów
	 */
	private int numberOfGenes;
	/**
	 * Długość pojedynczego genu (w bitach)
	 */
	private int lengthOfGene;
	/**
	 * Długość w bitach całego genomu
	 */
	private int lengthOfGenome;
	
	
	/**
	 *  Konstruktor chromosomu o określonej długości i wartościach ustalanych na podstawie BitSetu będącego parametrem
	 * @param numberOfGenes
	 * @param lengthOfGene
	 * @param genome
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final BitSet genome) {
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.genome = (BitSet) genome.clone();
		this.lengthOfGenome = this.numberOfGenes * this.lengthOfGene;
	}
	
	/**
	 * Konstruktor chromosomu o określonej długości i równym prawdopodobieństwie 0 i 1 dla każdego bitu
	 * @param numberOfGenes
	 * @param lengthOfGene
	 * @param random
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final Boolean random) {
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.lengthOfGenome = this.numberOfGenes * this.lengthOfGene;
		this.genome = new BitSet(lengthOfGenome);
		if (random == true) {
			RandomGenerator rg = Enviroment.getRandomGenerator();
			for(int i = 0; i < lengthOfGenome; i++) {
				genome.set(i, rg.nextBoolean());
			}
		}
	}
	
	/**
	 * Konstruktor chromosomu o określonej długości i określonym  przez parametr prawdopodobieństwie 1 dla każdego bitu 
	 * @param numberOfGenes
	 * @param lengthOfGene
	 * @param random
	 * @param probabilityOfTrue
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final Boolean random, double probabilityOfTrue) {
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.lengthOfGenome = this.numberOfGenes * this.lengthOfGene;
		this.genome = new BitSet(lengthOfGenome);
		if (random == true) {
			RandomGenerator rg = Enviroment.getRandomGenerator();
			for(int i = 0; i < lengthOfGenome; i++) {
				if (rg.nextDouble() < probabilityOfTrue) {
					genome.set(i, true);
				}
				else {
					genome.set(i, false);
				}
			}
		}
	}
	
	/**
	 * Konstruktor chromosomu o określonej długości i wartościach ustalanych na podstawie odpowiedniego napisu
	 * @param numberOfGenes
	 * @param lengthOfGene
	 * @param chromosomeRepresentation
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final String chromosomeRepresentation) {
		this.genome = new BitSet();
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.lengthOfGenome = this.numberOfGenes * this.lengthOfGene;
		
		if (chromosomeRepresentation.length() != this.lengthOfGenome) {
			throw new IllegalArgumentException("chromosomeRepresentation.length() != numberOfGenes * lengthOfGene");
		}		
		
		for (int i = 0; i < this.lengthOfGenome; i++) {
			int element = Integer.parseInt(Character.toString(chromosomeRepresentation.charAt(i)));
			if(element == 0) {
				genome.set(i, false);
			}
			else if(element == 1) {
				genome.set(i);
			}
			else {
				throw new IllegalArgumentException("Chromosome representation should contain only '0' and '1'");
			}
		}
	}
	
	/**
	 * Konstruktor kopiujący
	 * @param other
	 */
	public Chromosome(final Chromosome other) {
		this.lengthOfGene = other.lengthOfGene;
		this.numberOfGenes = other.numberOfGenes;
		this.lengthOfGenome = other.lengthOfGenome;
		this.genome = (BitSet) other.genome.clone();
	}
	
	
	/**
	 * Realizacja krzyżowania równomiernego na poziomie chromosomoów z użyciem zadanej maski
	 * @param other
	 * @param mask
	 * @return
	 */
	public Chromosome crossover(final Chromosome other, final Chromosome mask) {
		if((this.numberOfGenes != other.numberOfGenes) | (this.lengthOfGene != other.lengthOfGene)) {
			throw new IllegalArgumentException("numberOfGenes and this.lengthOfGene should be equal in both chromosomes");
		}
		BitSet t1 = (BitSet) this.genome.clone();
		BitSet t2 = (BitSet) other.genome.clone();
		BitSet mg = mask.genome;
		t1.andNot(mg);
		t2.and(mg);
		t1.or(t2);
		Chromosome newChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene, false);
		newChromosome.setGenome(t1);
		return newChromosome;
	}
	
	/**
	 * Realizacja oparatora mutacji na poziomie chromosomów
	 * @param probability
	 * @return
	 */
	public Chromosome mutate(double probability) {
		Chromosome mutatedChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene, false);
		BitSet newChromosome = (BitSet) this.getGenome().clone();
		RandomGenerator rg = Enviroment.getRandomGenerator();
		for(int i = 0; i < this.lengthOfGenome; i++) {
			if(rg.nextDouble() < probability) {
				newChromosome.flip(i);
			}
		}
		mutatedChromosome.setGenome(newChromosome);
		return mutatedChromosome;
	}
	
	/**
	 * Realizacja oparatora mutacji na poziomie chromosomów w odniesieniu do wybranego genu
	 * @param probability
	 * @return
	 */
	public Chromosome mutate(double probability, int numberOfGene) {
		Chromosome mutatedChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene, false);
		BitSet newChromosome = (BitSet) this.getGenome().clone();
		RandomGenerator rg = Enviroment.getRandomGenerator();
		for(int i = numberOfGene * this.lengthOfGene; i < (numberOfGene + 1) * this.lengthOfGene; i++) {
			if(rg.nextDouble() < probability) {
				newChromosome.flip(i);
			}
		}
		mutatedChromosome.setGenome(newChromosome);
		return mutatedChromosome;
	}
	
	/**
	 * Dekodowanie genotyp - fenotyp w zadanej domenie poszukiwań
	 * @return tablica wartości fenotypu
	 */
	public double[] decodeChromosme() {
		double decoded[] = new double [this.numberOfGenes];
		double decodedGene = 0;
		int geneIndex = this.lengthOfGene-1;
		int geneNumber = 0;
		for (int i = 0; i < this.lengthOfGenome; i++, geneIndex--) {
			Boolean test = this.genome.get(i);
			if	(test == true) {
				decodedGene += Math.pow(2, geneIndex);
			}
			if(geneIndex == 0) {
				decoded[geneNumber] = decodedGene / (Math.pow(2, this.lengthOfGene)-1) * 2 * domain - domain;
				decodedGene = 0;
				geneIndex = this.lengthOfGene;
				geneNumber++;
			}
			
		}
		return decoded;
	}
	
	
	/**
	 * Uzyskanie wartości fenotypu dla wybranego genu
	 * @param numberOfGene
	 * @return
	 */
	public double getDecodedGene(int numberOfGene) {
		return this.decodeChromosme()[numberOfGene];
	}
	
	/**
	 * Uzsykanie reprezentacji wybranego genu
	 * @param numberOfGene
	 * @return
	 */
	public String getGene(int numberOfGene) {
		return(this.toString().split("; ")[numberOfGene]);
	}
	
	/**
	 * Ewaluacja przystosowania z użyciem funckji Ackleya. 
	 * @return
	 */
	public double evaluateChromosome(){
		double sum1 = 0;
		double sum2 = 0;
		double  multplier = 1.0/(double)numberOfGenes;
		for (double l : this.decodeChromosme()) {
			sum1 += Math.pow(l, 2);
			sum2 += Math.cos(2.0 * Math.PI * l);
		}
		return   Math.exp(1) + 20 - 20.0 * Math.exp(-0.2 * Math.sqrt(multplier * sum1)) - Math.exp(multplier * sum2); 
	}
	
	/**
	 * Reprezentacja fenotypu w postaci napisu
	 * @return
	 */
	public String decodedToString() {
		double decoded[] = this.decodeChromosme();
		String decodedString = "";
		for (int i = 0; i < decoded.length; i++) {
			decodedString += decoded[i];
			if(i != (decoded.length - 1)) {
				decodedString += "; ";
			}
		}
		return decodedString + "";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String chromosomeRepresentation = "";
		int geneIndex = 0;
		int geneNumber = 0;
		for (int i = 0; i < this.lengthOfGenome; i++, geneIndex++) {
			if (geneIndex == this.lengthOfGene) {
				chromosomeRepresentation += "; ";
				geneIndex = 0;
				geneNumber++;
			}
			if(genome.get(i) == true) chromosomeRepresentation += "1";
			if(genome.get(i) == false)  chromosomeRepresentation += "0";
		}
		return chromosomeRepresentation + "";
	}

	public BitSet getGenome() {
		return (BitSet) genome.clone();
	}

	public void setGenome(BitSet genome) {
		this.genome = (BitSet) genome.clone();
	}

	public int getNumberOfGenes() {
		return numberOfGenes;
	}

	public int getLengthOfGene() {
		return lengthOfGene;
	}

}
