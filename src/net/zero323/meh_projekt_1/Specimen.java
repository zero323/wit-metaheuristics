package net.zero323.meh_projekt_1;

/**
 * @author Maciej Szymkiewicz
 * Klasa reprezentująca pojedyńczego osobnika
 *
 */
public class Specimen implements Comparable<Specimen> {
	/**
	 * Chromosom 
	 */
	private Chromosome chromosome;
	/**
	 * Wiek osobnika
	 */
	private int age;
	/**
	 * Wartość funkcji przystosowania
	 */
	private double fitness;
	
	/**
	 * Konstruktor tworzący osobnika o losowym genomie o określonej długości
	 * @param numberOfGenes liczba genów
	 * @param lengthOfGene długość pojedyńczego genu
	 */
	public Specimen(final int numberOfGenes, final int lengthOfGene) {
		this.chromosome = new Chromosome(numberOfGenes, lengthOfGene, true);
		this.age = 0;
		this.fitness = this.chromosome.evaluateChromosome();
	}
	
	/**
	 * Konstuktor kopiujący
	 * @param other osobnik na podstawie którego tworzymy nowego osobnika 
	 */
	public Specimen(Specimen other) {
		this.chromosome = other.chromosome;
		this.age = 0;
		this.fitness = other.fitness;
	}
	
	/**
	 * Konstruktor tworzący osobnika z zadanym genomem 
	 * @param numberOfGenes liczba genów
	 * @param lengthOfGene długość pojedyńczego genu
	 * @param chromosome chromosm tworzonego osobnika
	 */
	public Specimen(final int numberOfGenes, final int lengthOfGene, Chromosome chromosome) {
		this.chromosome = chromosome;
		this.age = 0;
		this.fitness = this.chromosome.evaluateChromosome();
	}	
	
	/**
	 * Metoda odpowiadajaca za krzyżowanie osobników.
	 * @param other drugi rodzic
	 * @param mask maska krzyżowania określająca czy dojdzie do wymiany danego bitu w genomie.
	 * @return nowy osobnik bedący efektem krzyżowania równomiernego dwóch osobników przy użyciu okreslonej maski
	 */
	public Specimen crossbreed(final Specimen other, final Chromosome mask) {
		Chromosome newChromosome = this.chromosome.crossover(other.chromosome, mask);
		return  new Specimen(this.chromosome.getNumberOfGenes(), this.chromosome.getLengthOfGene(), newChromosome);
	}
	
	/**
	 * Metoda wywołująca mutację chromosomu danego osobnika
	 * @param probability prawdopodobieństwo mutacji każdego z bitów genomu
	 * @return nowy osobnik o genomie będącym wynikiem mutacji genomu osobnika początkowego
	 */
	public Specimen mutate(double probability) {
		Chromosome newChromosome = this.chromosome.mutate(probability);
		return  new Specimen(this.chromosome.getNumberOfGenes(), this.chromosome.getLengthOfGene(), newChromosome);
	}
	
	/**
	 * Zwraca starszego o pokelenie osobnika będącego kopia aktualnego/ 
	 * @return
	 */
	public Specimen incrementAge() {
		Specimen specimen = new Specimen(this);
		specimen.age++;
		return specimen;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Specimen [fenotype: " + this.fenotypeToString() + ", fitness=" + fitness + "]";
	}
	
	/**
	 * Funkcja zwracająca napis będący reprezentacją fenotypu
	 * @return
	 */
	public String fenotypeToString(){
		String line = this.chromosome.decodedToString();
		return line;
	}
	
	/**
	 * Funkcja zwracająca napis będący reprezentacją genotypu
	 * @return
	 */
	public String genotypeToString(){
		String line = this.chromosome.toString();
		return line;
	}
	
	/**
	 * Funkcja zwracająca napis będący reprezentacją fenotypu dla wybranego genu
	 * @param numberOfGene numer żadanego genu
	 * @return napis będący reprezentacją fragmentu fenotypu
	 */
	public String fenotypeToString(int numberOfGene){
		String line = Double.toString(this.chromosome.getDecodedGene(numberOfGene));
		return line;
	}

	/**
	 * Funkcja zwracająca napis będący reprezentacją  wybranego genu
	 * @param numberOfGene numer żądanego genu
	 * @return napis bedący reprezentacją genu
	 */
	public String genotypeToString(int numberOfGene){
		String line = this.chromosome.getGene(numberOfGene);
		return line;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Specimen other) {
		if (this.fitness - other.fitness < 1)
			return -1;
		else if (this.fitness - other.fitness > 1)
			return 1;
		else
			return 0;
	}


	/**
	 * Getter dla funkcji przystosowania
	 * @return wartość funkcji przystosowania dla danego osobnika
	 */
	public double getFitness() {
		return fitness;
	}
}
