package net.zero323.meh_projekt_2;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Maciej Szymkiewicz
 * 
 */
public class Main {
	private static int numberOfSpecimens;
	private static int numberOfEliteSpecimens;
	private static int numberOfSpecimensInTournament;
	private static int envNumberOfGenes;
	private static int envLengthOfGene;
	private static int maxNumberOfIterations;
	private static int maxNumberOfIterationsWithoutNewSolution;
	private static double probabilityOfMutation;
	private static int numberOfTestIterations;
	private static double probabilityOfSwapping;
	private static String forbbidenFields = null;
	private static double probOfForbiddenField;
	
	public static void main(String[] args) {
		defaultSettings();
		run(args);
	}
	
	public static void run (String[] args) {
		if(args.length > 0) {
			if (args[0].equals("test")) {
				if (args.length == 2) {
					numberOfTestIterations = Integer.parseInt(args[1]);
				}
				defaultSettings();
				runTests(numberOfTestIterations);
			}
			
			else if (args[0].equals("properties")) {
				getProperties();
				Enviroment e = getEnviroment();
				if(args.length == 2) {
					if(args[1].equals("printSolutions")) {
						e.testEnviromentSettings(1, true);
					}
				}
				else {
					e.testEnviromentSettings(1, false);
				}
			}
			
			else if (args[0].equals("default")) {
				defaultSettings();
				Enviroment e = getEnviroment();
				if(args.length == 2) {
					if(args[1].equals("printSolutions")) {
						e.testEnviromentSettings(1, true);
					}
				}
				else {
					e.testEnviromentSettings(1, false);
				}
			}
		}
	}
	
	static public void defaultSettings(){
		numberOfSpecimens = 100;
		numberOfEliteSpecimens = (int)(numberOfSpecimens * 0.05);
		numberOfSpecimensInTournament = (int)(numberOfSpecimens * 0.1);
		envNumberOfGenes = 50;
		envLengthOfGene = 50;
		maxNumberOfIterations = 250000;
		maxNumberOfIterationsWithoutNewSolution = 100000;
		probabilityOfMutation = 0.05;
		numberOfTestIterations = 10;
		probabilityOfSwapping = 0.20;
		probOfForbiddenField = 0.15;
		if (probOfForbiddenField > 0) {
			forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
		}
		else {
			forbbidenFields = null;
		}
	}
	
	public static Enviroment getEnviroment() {
		return new Enviroment(
				numberOfSpecimens,
				numberOfEliteSpecimens,
				numberOfSpecimensInTournament,
				envNumberOfGenes,
				envLengthOfGene,
				maxNumberOfIterations,
				maxNumberOfIterationsWithoutNewSolution,
				probabilityOfMutation,
				probabilityOfSwapping,
				forbbidenFields);
	}
	
	public static void getProperties() {
		Properties prop = new Properties();
		try {
	        FileInputStream inputStream = new FileInputStream("./meh.properties");
	        prop.load(inputStream);
	        numberOfSpecimens = Integer.parseInt(prop.getProperty("numberOfSpecimens"));
	        numberOfEliteSpecimens = Integer.parseInt(prop.getProperty("numberOfEliteSpecimens"));
	        numberOfSpecimensInTournament = Integer.parseInt(prop.getProperty("numberOfSpecimensInTournament"));
	        envNumberOfGenes = Integer.parseInt(prop.getProperty("envNumberOfGenes"));
	        envLengthOfGene = Integer.parseInt(prop.getProperty("envLengthOfGene"));
	        maxNumberOfIterations = Integer.parseInt(prop.getProperty("maxNumberOfIterations"));
	        maxNumberOfIterationsWithoutNewSolution = Integer.parseInt(prop.getProperty("maxNumberOfIterationsWithoutBetterFitness"));
	        probabilityOfMutation = Double.parseDouble(prop.getProperty("probabilityOfMutation"));
	        probabilityOfSwapping = Double.parseDouble(prop.getProperty("probabilityOfSwapping"));
	        numberOfTestIterations = Integer.parseInt(prop.getProperty("numberOfTestIterations"));
	        probOfForbiddenField = Double.parseDouble(prop.getProperty("probOfForbiddenField"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}   
		
	}
	
	public static void runTests(int numberOfIterations) {
		/* Test dla różnej wielkości populacji */
		System.out.println(
			"##############################################################\n" +
			"#        Test dla różnej wielkości populacji                 #\n" +
			"##############################################################\n\n");
		defaultSettings();
		int optNumberOfSpecimens[] = {50, 100, 150, 200};
		for (int i : optNumberOfSpecimens) {
			numberOfSpecimens = i;
			numberOfTestIterations = 5;
			numberOfEliteSpecimens = (int)(numberOfSpecimens * 0.10);
			numberOfSpecimensInTournament = (int)(numberOfSpecimens * 0.10);
			
			if (probOfForbiddenField > 0) {
				forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
			}
			else {
				forbbidenFields = null;
			}
			
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnej wielkości populacji elitarnej */ 
		System.out.println(
				"##############################################################\n" +
				"#       Test dla różnej wielkości populacji elitarnej        #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optPercentOfEliteSpecimens[] = {0.0, 0.10, 0.50, 0.75};
		for (double d : optPercentOfEliteSpecimens) {
			numberOfEliteSpecimens = (int)(numberOfSpecimens * d);
			
			if (probOfForbiddenField > 0) {
				forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
			}
			else {
				forbbidenFields = null;
			}
			
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnej wielkości grupy turniejowej */
		System.out.println(
				"##############################################################\n" +
				"#       Test dla różnej wielkości grupy turniejowej          #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optPercentOfSpecimensInTournament[] = {0.05, 0.10, 0.50, 0.75};
		for (double d : optPercentOfSpecimensInTournament) {
			numberOfSpecimensInTournament = (int)(numberOfSpecimens * d);
			
			if (probOfForbiddenField > 0) {
				forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
			}
			else {
				forbbidenFields = null;
			}
			
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnych wartości prawdopodobieństwa mutacji*/ 
		System.out.println(
				"##############################################################\n" +
				"#   Test dla różnych wartości prawdopodobieństwa mutacji     #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optProbabilityOfMutation[] = {0.0, 0.05, 0.1, 0.25, 0.50, 0.75, 0.9, 1.00};
		for (double d : optProbabilityOfMutation) {
			probabilityOfMutation = d;
			
			if (probOfForbiddenField > 0) {
				forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
			}
			else {
				forbbidenFields = null;
			}
			
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		/* Test dla różnych wartości prawdopodobieństw zamiany genów*/ 
		System.out.println(
				"##############################################################\n" +
				"#  Test dla różnych wartości prawdopodobieństw zamiany genów   #\n" +
				"#  genów                                                     #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optprobabilityOfSwapping [] = {0.0, 0.05, 0.1, 0.25, 0.50, 0.75, 0.9, 1.00};
		for (double d : optprobabilityOfSwapping ) {
			probabilityOfSwapping = d;
			if (probOfForbiddenField > 0) {
				forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(probOfForbiddenField, envNumberOfGenes, envLengthOfGene);
			}
			else {
				forbbidenFields = null;
			}
			
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}
		
		
		
		/* Test dla różnych zestwów pól zakazanych*/ 
		
		System.out.println(
				"##############################################################\n" +
				"#  Test dla różnych zestwów pól zakazanych                    #\n" +
				"#                                                            #\n" +
				"##############################################################\n\n");
		defaultSettings();
		double optForbiddenFieldPropbability [] = {0.1, 05, 0.10, 0.25, 0.50, 0.75};
		for (double d : optForbiddenFieldPropbability ) {
			forbbidenFields = ForbiddenFieldsGenerator.getForbiddenFields(d, envNumberOfGenes, envLengthOfGene);
			Enviroment e = getEnviroment();
			e.testEnviromentSettings(numberOfIterations, false);
		}	
	}

}
