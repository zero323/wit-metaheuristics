package net.zero323.meh_projekt_2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.math.random.RandomGenerator;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * @author Maciej Szymkiewicz
 * Klasa będąca reprezentacją genoptypu
 *
 */
public class Chromosome {
	
	/**
	 * Srodowisko chromosomu
	 */
	private static Enviroment enviroment;
	
	/**
	 * Tablica reprezentująca genom 
	 */
	private Integer[] genome;
	/**
	 * Liczba genów (wierszy)
	 */
	private int numberOfGenes;
	/**
	 * Maksymalna wartość genu (liczba kolumn)
	 */
	private int lengthOfGene;
	
	
	
	/**
	 * Liczba królowych w kolumnach 
	 */
	private int[] queensInColumn;
	/**
	 * Liczba królowych w pozytywnych diagonalach
	 */
	private Map<Integer, Integer> queensInPositiveDiagonal;
	/**
	 * Liczba królowych w negatywnych diagonalach
	 */
	private Map <Integer, Integer> queensInNegativeDiagonal;

	
	
	/**
	 * Konflikty w kolumnach
	 */
	private int columnsConflicts;
	/**
	 * Konflikty na pozytywnych diagonalach
	 */
	private int positiveDiagonalsConflicts;

	/**
	 * Konflikty na negatywnych diagonalach
	 */
	private int negativeDiagonalsConflicts;
	
	
	
	/**
	 * Liczba konfliktów w kolumnach
	 */
	private int numberOfConflictsInColumns = 0;
	/**
	 * Liczba konfliktów na pozytywnych diagonalach
	 */
	private int numberOfConflictsOnPositiveDiagonals = 0;
	/**
	 * Liczba konfliktów na negatywnych diagonalach
	 */
	private int numberOfConflictsOnNegativeDiagonals = 0;
	/**
	 * Liczba konfliktów na polach zakazanych
	 */
	private int numberOfConflictsOnForbiddenFields = 0;

	/**
	 * Liczba królowych
	 */
	private int numberOfQueens = 0;

	/**
	 *  Konstruktor chromosomu o określonej długości i wartościach ustalanych na podstawie otrzymanej tablicy
	 * @param lengthOfGene
	 * @param genome
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final Integer[] genome) {
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.genome = genome.clone();
		this.queensInColumn = new int[numberOfGenes];
		this.queensInPositiveDiagonal = new HashMap<Integer, Integer>();
		this.queensInNegativeDiagonal =  new HashMap<Integer, Integer>();
	}
	
	/**
	 * Konstruktor losowego chromosomu o określonej długości.
	 * @param lengthOfGene
	 * @param random
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene) {
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		this.genome = new Integer[numberOfGenes];
		RandomGenerator rg = Enviroment.getRandomGenerator();
		for(int i = 0; i < numberOfGenes; i++) {
			genome[i] = rg.nextInt(lengthOfGene);
		}
		this.queensInColumn = new int[this.lengthOfGene];
		this.queensInPositiveDiagonal = new HashMap<Integer, Integer>();
		this.queensInNegativeDiagonal =  new HashMap<Integer, Integer>();
	}
	
	/**
	 * Konstruktor chromosomu o określonej długości i wartościach ustalanych na podstawie odpowiedniego napisu
	 * @param numberOfGenes
	 * @param lengthOfGene
	 * @param chromosomeRepresentation
	 */
	public Chromosome(final int numberOfGenes, final int lengthOfGene, final String chromosomeRepresentation) {
		this.genome = new Integer[numberOfGenes];
		this.numberOfGenes = numberOfGenes;
		this.lengthOfGene = lengthOfGene;
		
		if (chromosomeRepresentation.length() != this.numberOfGenes) {
			throw new IllegalArgumentException("chromosomeRepresentation.length() != this.numberOfGenes");
		}		
		
		for (int i = 0; i < this.numberOfGenes; i++) {
			int element = Integer.parseInt(Character.toString(chromosomeRepresentation.charAt(i)));
			genome[i] = element;
		}
		
		this.queensInColumn = new int[this.lengthOfGene];
		this.queensInPositiveDiagonal = new HashMap<Integer, Integer>();
		this.queensInNegativeDiagonal =  new HashMap<Integer, Integer>();
	}
	
	/**
	 * Konstruktor kopiujący
	 * @param other
	 */
	public Chromosome(final Chromosome other) {
		this.lengthOfGene = other.lengthOfGene;
		this.numberOfGenes = other.numberOfGenes;
		this.genome = (Integer[]) other.genome.clone();
		
		this.queensInColumn = new int[this.lengthOfGene];
		this.queensInPositiveDiagonal = new HashMap<Integer, Integer>();
		this.queensInNegativeDiagonal =  new HashMap<Integer, Integer>();

	}
	
	
	/**
	 * Realizacja krzyżowania 
	 * @param other
	 * @return
	 */
	public Chromosome crossover(final Chromosome other) {
		if((this.numberOfGenes != other.numberOfGenes) | (this.lengthOfGene != other.lengthOfGene)) {
			throw new IllegalArgumentException("numberOfGenes and this.lengthOfGene should be equal in both chromosomes");
		}
		RandomGenerator rg = Enviroment.getRandomGenerator();
		int cuttingIndex =  rg.nextInt(this.numberOfGenes);
		Chromosome newChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene);
		for (int i = 0; i < this.numberOfGenes; i++) {
			if(i <= cuttingIndex) {
				newChromosome.genome[i] = this.genome[i];
			}
			else {
				newChromosome.genome[i] = other.genome[i];
			}
			
		}
		return newChromosome;
	}
	
	
	/**
	 * Realizacja oparatora mutacji na poziomie chromosomów
	 * @param probability
	 * @return
	 */
	public Chromosome mutate(double probability) {
		Chromosome mutatedChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene);
		Integer[] newChromosome = (Integer[]) this.getGenome().clone();
		RandomGenerator rg = Enviroment.getRandomGenerator();
		for(int i = 0; i < this.numberOfGenes; i++) {
			if(rg.nextDouble() < probability) {
				int newGeneValue = rg.nextInt(this.lengthOfGene);
				if (newGeneValue == this.lengthOfGene) {
					newChromosome[i] = null;
				}
				else {
					newChromosome[i] = newGeneValue;
				}
			}
		}
		mutatedChromosome.setGenome(newChromosome);
		return mutatedChromosome;
	}
	
	
	public Chromosome swapMutate(double probabilityOfSwap) {
		Chromosome mutatedChromosome = new Chromosome(this.numberOfGenes, this.lengthOfGene);
		Integer[] newChromosome = (Integer[]) this.getGenome().clone();
		Integer swapedValue;
		int indexOfSwapped;
		RandomGenerator rg = Enviroment.getRandomGenerator();
		for(int i = 0; i < this.numberOfGenes; i++) {
			if(rg.nextDouble() < probabilityOfSwap) {
				swapedValue = mutatedChromosome.genome[i];
				indexOfSwapped = rg.nextInt(this.numberOfGenes);
				mutatedChromosome.genome[i]  = mutatedChromosome.genome[indexOfSwapped];
				mutatedChromosome.genome[indexOfSwapped] = swapedValue;
				
			}
		}
		mutatedChromosome.setGenome(newChromosome);
		return mutatedChromosome;
	}
	
	
	
	/**
	 * Uzsykanie reprezentacji wybranego genu
	 * @param numberOfGene
	 * @return
	 */
	public String getGene(int numberOfGene) {
		//TODO
		return(this.toString().split("; ")[numberOfGene]);
	}
	
	/**
	 * Ewaluacja przystosowania
	 * @return
	 */
	public double evaluateChromosome(){
		this.numberOfConflictsOnPositiveDiagonals = 0;
		this.numberOfConflictsOnNegativeDiagonals = 0;
		this.numberOfConflictsInColumns = 0;
		this.numberOfConflictsOnForbiddenFields = 0;
		this.numberOfQueens = 0;
		
		for(int i=0; i < this.numberOfGenes; i++){
			if (this.genome[i] != null) {
				if(this.queensInPositiveDiagonal.containsKey(this.genome[i] - i)) {
					this.queensInPositiveDiagonal.put(this.genome[i] - i, this.queensInPositiveDiagonal.get(this.genome[i] - i) + 1);
				}
				else {
					this.queensInPositiveDiagonal.put(this.genome[i] - i, 1);
				}
				
				if(this.queensInNegativeDiagonal.containsKey(this.genome[i] + i)) {
					this.queensInNegativeDiagonal.put(this.genome[i] + i, this.queensInNegativeDiagonal.get(this.genome[i] + i) + 1);
				}
				else {
					this.queensInNegativeDiagonal.put(this.genome[i] + i, 1);
				}
				this.queensInColumn[this.genome[i]] += 1;
			}
		}
		
		for(int i: this.queensInNegativeDiagonal.values()) {
				this.numberOfConflictsOnNegativeDiagonals += i - 1;
		}
		
		for(int i: this.queensInPositiveDiagonal.values()) {
			if(i > 1) {
				this.numberOfConflictsOnPositiveDiagonals += i - 1;
			}
		}
		
		for(int i: queensInColumn) {
			if(i > 1) {
				this.numberOfConflictsInColumns += i - 1;
			}
		}
		
		for (int i = 0; i < this.numberOfGenes; i++) {
			if(this.genome[i] != null) {
				if(enviroment.getForbiddenFields()[i][this.genome[i]] == true) {
					this.numberOfConflictsOnForbiddenFields++;
				}
			}
		}
		if (this.numberOfConflictsInColumns - enviroment.getFitnessCorrrect() <= 0) {
			this.numberOfConflictsInColumns = 0;
		}
		else this.numberOfConflictsInColumns = this.numberOfConflictsInColumns - enviroment.getFitnessCorrrect();
		
		return (numberOfConflictsInColumns +
				numberOfConflictsOnForbiddenFields + 
				numberOfConflictsOnNegativeDiagonals + 
				numberOfConflictsOnPositiveDiagonals);
	}
	
	/**
	 * Reprezentacja fenotypu w postaci napisu
	 * @return
	 */
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	
	public String toString() {
		String decodedString = "";
		for (int i = 0; i < this.numberOfGenes; i++) {
			if(this.genome[i] != null) {
				decodedString += this.genome[i].toString();
			}
			else {
				decodedString += "-";
			}
			decodedString += "; ";
		}
		
		return decodedString + "";
	}
	

	public Integer[] getGenome() {
		return (Integer[]) genome.clone();
	}

	public void setGenome(Integer[] genome) {
		this.genome = (Integer[]) genome.clone();
	}

	public int getNumberOfGenes() {
		return numberOfGenes;
	}

	public int getLengthOfGene() {
		return lengthOfGene;
	}
	
	public static void setEnviroment(Enviroment env){
		Chromosome.enviroment = env;
	}

}
