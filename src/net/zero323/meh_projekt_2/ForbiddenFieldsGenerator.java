package net.zero323.meh_projekt_2;

import java.util.Date;

import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

public class ForbiddenFieldsGenerator {
	private static RandomGenerator  rg = new Well44497b(new Date().getTime());
	public static String getForbiddenFields(double probability, int x, int y) {
		String s = "";
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				if(rg.nextDouble() <= probability) 
					s += i + " " + j + ","; 
			}
		}
		return s;
	}

}
