package net.zero323.meh_projekt_2;

import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Maciej Szymkiewicz
 * Klasa reprezentująca środowisko działania algorytmu
 *
 */
public class Enviroment {
	
	private static RandomGenerator rg;
	private int numberOfSpecimens;
	private int numberOfEliteSpecimens;
	private int numberOfSpecimensInTournament;
	private int envNumberOfGenes;
	private int envLengthOfGene;
	private boolean[][] forbiddenFields;
	private int MaxNumberOfQueens;
	private int fitnessCorrrect;
	private int numberOfSolutions;

	private long maxNumberOfIterations;
	private long maxNumberOfIterationsWithoutNewSolution;
	private Integer iteration;
	private Integer iterationOfTheBestSolution;
	private long iterationsWithoutNewSolution;
	private double theBestSolution;

	private double probabilityOfMutation;
	private double probabilityOfSwapping;
	private Specimen population[];
	private ArrayList<Specimen> populationWithoutSolutions;
	private Specimen tournamentPopulation[];
	private Specimen populationAfterTournament[];
	private Specimen subpopulationOfEliteSpecimens[];
	private Specimen offspringPopulation[];
	private HashSet<String> solutions;

	/**
	 * Konstruktor klasy
	 * @param numberOfSpecimens - liczba osobników w populacji
	 * @param numberOfEliteSpecimens - liczba najlepszych osobników z populacji bazowej wybierana do sukcesji elitarnej
	 * @param numberOfSpecimensInTournament - liczba osobników w turnieju
	 * @param envNumberOfGenes - liczba genów (wierszy na szachownicy)
	 * @param envLengthOfGene - Liczba kolumn na szachownicy
	 * @param maxNumberOfIterations - maksymalna liczba iteracji
	 * @param maxNumberOfIterationsWithoutNewSolution- maksymalna liczba iteracji bez uzyskania nowego rozwiązania
	 * @param probabilityOfMutation - prawdopodobieństwo mutacji pojedyńczego genu.
	 * @param probabilityOfSwapping - pradwpodobieństwo zamiany genów
	 */
	public Enviroment(
			final int numberOfSpecimens,
			final int numberOfEliteSpecimens,
			final int numberOfSpecimensInTournament,
			final int envNumberOfGenes,
			final int envLengthOfGene,
			final long maxNumberOfIterations,
			final long maxNumberOfIterationsWithoutNewSolution,
			final double probabilityOfMutation,
			final double probabilityOfSwapping,
			final String forbiddenFields
			) {
		this.numberOfSpecimens = numberOfSpecimens;
		this.numberOfEliteSpecimens = numberOfEliteSpecimens;
		if (numberOfSpecimensInTournament < 1) {
			throw new IllegalArgumentException("numberOfSpecimensInTournament should be >= 1"); 
		}
		else {
			this.numberOfSpecimensInTournament = numberOfSpecimensInTournament;
		}
		if(envNumberOfGenes >= envLengthOfGene) {
			this.envNumberOfGenes = envNumberOfGenes;
			this.envLengthOfGene = envLengthOfGene;
		}
		else {
			throw new NotImplementedException();
		}
		this.maxNumberOfIterations = maxNumberOfIterations;
		this.maxNumberOfIterationsWithoutNewSolution = maxNumberOfIterationsWithoutNewSolution;
		this.iteration = 0;
		this.iterationOfTheBestSolution = null;
		this.iterationsWithoutNewSolution = 0;
		this.theBestSolution = 99999999;
		this.probabilityOfMutation = probabilityOfMutation;
		this.probabilityOfSwapping = probabilityOfSwapping;
		this.population = new Specimen[this.numberOfSpecimens];
		this.subpopulationOfEliteSpecimens = new Specimen[numberOfEliteSpecimens];
		this.tournamentPopulation = new Specimen[numberOfSpecimensInTournament];
		this.populationAfterTournament = new Specimen[numberOfSpecimens];
		this.offspringPopulation = new Specimen[numberOfSpecimens];
		this.solutions = new HashSet<String>();
		this.numberOfSolutions = 0;
		this.forbiddenFields = new boolean[this.envNumberOfGenes][this.envLengthOfGene];
		
		
		int[] forbiddenFieldsInRow = new int[this.envNumberOfGenes];
		int[] forbiddenFieldsInColumns = new int[this.envLengthOfGene];
		
		int aviableRows = this.envNumberOfGenes;
		int aviableColumns = this.envLengthOfGene;
		
		if (forbiddenFields != null) {
			                                     
			String[] splitedFields = forbiddenFields.split(",");
			for(String f: splitedFields){
				String field[] = f.trim().split(" ");
				int row = Integer.parseInt(field[0].trim());
				int column = Integer.parseInt(field[1].trim());
				this.forbiddenFields[row][column] = true;
				forbiddenFieldsInColumns[column]++;
				forbiddenFieldsInRow[row]++;
			}
			
			for (int row : forbiddenFieldsInRow) {
				if(row == this.envLengthOfGene) {
					aviableRows--;
				}
			}
			
			for (int column : forbiddenFieldsInColumns) {
				if(column == this.envNumberOfGenes) {
					aviableColumns--;
				}
			}
		}
		
		if(aviableColumns < aviableRows) {
			this.MaxNumberOfQueens = aviableColumns;
		}
		else {
			this.MaxNumberOfQueens = aviableRows;
		}
		
		this.fitnessCorrrect = this.envLengthOfGene - this.MaxNumberOfQueens;
		
		Chromosome.setEnviroment(this);
	}
	
	public int getFitnessCorrrect() {
		return fitnessCorrrect;
	}

	/**
	 * Tworzenie losowej populacji początkowej
	 */
	public void populate() {
		for (int i = 0; i < this.numberOfSpecimens; i++){
			this.population[i] = new Specimen(this.envNumberOfGenes, this.envLengthOfGene);
		}
	}
	
	/**
	 * Realizacja turnieju przy populacji tymczasowej równej populacji wyjściowej, losowanie ze zwracaniem
	 * Pomijanie osobników o przystosowaniu równym 0 (znalezionych rozwiązań)
	 */

	public void tournament() {
		this.populationWithoutSolutions = new ArrayList<Specimen>();
		for (Specimen specimen : this.population) {
			if(specimen.getFitness() != 0 ) {
				populationWithoutSolutions.add(specimen);
			}
			
		}
		int tempIndex = 0;
		ArrayList<Integer> listOfIndexes = new ArrayList<Integer>(this.populationWithoutSolutions.size());
		for(int i=0; i<this.numberOfSpecimens; i++) {
			for(int indexOfLOI=0; indexOfLOI<this.populationWithoutSolutions.size(); indexOfLOI++) {
				listOfIndexes.add(indexOfLOI);
			}
			for(int indexOfTP=0; indexOfTP<this.numberOfSpecimensInTournament; indexOfTP++) {
				tempIndex = getRandomGenerator().nextInt(listOfIndexes.size());
				this.tournamentPopulation[indexOfTP] = this.populationWithoutSolutions.get(listOfIndexes.get(tempIndex));
				listOfIndexes.remove(tempIndex);
			}
			Arrays.sort(tournamentPopulation, new SpecimenComparator());
			this.populationAfterTournament[i] = this.tournamentPopulation[0];
		}
	}
	
	/**
	 * Wybór najlepszych osobników (do sukcesji elitarnej) 
	 * Pomijanie osobników o przystosowaniu równym 0 (znalezionych rozwiązań)
	 */
	public void extractingEliteSpecimens() {
		Arrays.sort(population, new SpecimenComparator());
		for (int i = 0,  j = 0; i < this.numberOfEliteSpecimens; i++) {
			if (this.population[i].getFitness() == 0) {
				this.addSolution(this.population[i].fenotypeToString());
			}
			else {
				this.subpopulationOfEliteSpecimens[j] = this.population[i].incrementAge();
				j++;
			}
		}
	}
	
	/**
	 * Krzyżowanie osobników w populacji
	 */
	public void crossbreeding() {
		int iPAT;
		int i1;
		int i2;
		ArrayList<Integer> indexesOfPAT = new ArrayList<Integer>(this.populationAfterTournament.length);
		
		for (int i = 0; i < this.populationAfterTournament.length; i++) {
			indexesOfPAT.add(i);
		}
		
		while(indexesOfPAT.size() > 1) {
			iPAT = getRandomGenerator().nextInt(indexesOfPAT.size());
			i1 = indexesOfPAT.get(iPAT);
			indexesOfPAT.remove(iPAT);
			iPAT = getRandomGenerator().nextInt(indexesOfPAT.size());
			i2 = indexesOfPAT.get(iPAT);
			indexesOfPAT.remove(iPAT);
			
			offspringPopulation[i1] = populationAfterTournament[i1]
				.crossbreed(populationAfterTournament[i2]);

			offspringPopulation[i2] = populationAfterTournament[i2]
				.crossbreed(populationAfterTournament[i1]);
		}
	}
	
	
	/**
	 * Mutacje osobników potomnych 
	 */
	public void mutating() {
		for (int i = 0; i < offspringPopulation.length; i++) {
			offspringPopulation[i] = offspringPopulation[i].mutate(probabilityOfMutation);
			offspringPopulation[i] = offspringPopulation[i].swapMutate(probabilityOfSwapping);
		}
	}
	
	/**
	 * Sukcesja elitarna 
	 */
	public void succession() {
		Specimen outputPopulation[] = new Specimen[this.numberOfSpecimens + this.numberOfEliteSpecimens];
		int i = 0;
		for (Specimen specimen : this.offspringPopulation) {
			outputPopulation[i] = specimen;
			i++;		
		}
		for (Specimen specimen : this.subpopulationOfEliteSpecimens) {
			outputPopulation[i] = specimen;
			i++;		
		}
		Arrays.sort(outputPopulation, new SpecimenComparator());

		for (int j = 0; j < this.numberOfSpecimens; j++) {
			this.population[j] = outputPopulation[j];
		}
	}
	
	/**
	 * Ocena wyników iteracji 
	 */
	public void evaluate() {
		Arrays.sort(this.population, new SpecimenComparator());
		if(this.numberOfSolutions < this.solutions.size()) {
			this.iterationsWithoutNewSolution = 0;
			if (this.iterationOfTheBestSolution == null) {
				this.iterationOfTheBestSolution = this.iteration;
			}
			this.numberOfSolutions = this.solutions.size();
		}
		else {
			this.iterationsWithoutNewSolution +=1;
		}
		this.iteration++;
	}
	
	/**
	 * Funkcja pomocnicza agregująca kolejne etapy każdej iteracji
	 */
	public void doIteration() {
		this.extractingEliteSpecimens();
		this.tournament();
		this.crossbreeding();
		this.mutating();
		this.succession();
		this.evaluate();
	}
	
	
	/**
	 * Uruchomienie algorytmu
	 */
	public void startAlgorithm() {
		this.populate();
		for(int i = 0; i < this.maxNumberOfIterations; i++) {
			this.doIteration();
			if(this.iterationsWithoutNewSolution >= this.maxNumberOfIterationsWithoutNewSolution) {
				break;
			}
		}	
	}
	
	/**
	 * Przywrócenie początkowego stanu środowiska 
	 */
	public void resetEnviroment() {
		rg = null;
		this.iteration = 0;
		this.iterationsWithoutNewSolution= 0;
		this.theBestSolution = 99999999;
		this.iterationOfTheBestSolution = null;
		this.solutions = new HashSet<String>();
		this.numberOfSolutions = 0;
		this.population = new Specimen[this.numberOfSpecimens];
		this.subpopulationOfEliteSpecimens = new Specimen[this.numberOfEliteSpecimens];
		this.tournamentPopulation = new Specimen[this.numberOfSpecimensInTournament];
		this.populationAfterTournament = new Specimen[this.numberOfSpecimens];
		this.offspringPopulation = new Specimen[this.numberOfSpecimens];
	}
	
	/**
	 * Funkcja pomocnicza do drukowania wyników działania algorytmu
	 * @return
	 */
	public String header() {
		String header = "numberOfSpecimens; numberOfEliteSpecimens; numberOfSpecimensInTournament; " + 
						"probabilityOfMutation; probabilityOfSwapping; numberOfSolutions; iterationOfTheBestSolution";
		return header;
	}
	
	/**
	 * Funkcja pomocnicza do drukowania wyników działania algorytmu
	 * @return
	 */
	public String summary(final Boolean printPopulation) {
		String line = "";
		line += this.numberOfSpecimens + "; ";
		line += this.numberOfEliteSpecimens + "; ";
		line += this.numberOfSpecimensInTournament + "; ";
		line += this.probabilityOfMutation + "; ";
		line += this.probabilityOfSwapping + "; ";
		line += this.solutions.size() + "; ";
		line += this.iterationOfTheBestSolution;
		if(printPopulation) {
			return line + "\n\n" + 	this.printSolutions();
			}
		else {
			return line;
		}
	}
	
	/**
	 * Funkcja pomocnicza do drukowania struktury populacji
	 * @return
	 */
	public String populationToString() {
		String representation = "";
		for (int i = 0; i < this.envNumberOfGenes; i++) {
			representation += "x" + i + "; ";
		}
		representation += "fitness\n";
		
		for (Specimen specimen : population) {
			representation += specimen.fenotypeToString() + specimen.getFitness() + "\n";
		}
		return representation;
	}
	
	/**
	 * Funkcja pomocnicza
	 * @return
	 */
	public void testEnviromentSettings(final int iterations, final Boolean printPopulation) {
		System.out.println(this.header());
		for (int i = 0; i < iterations; i++) {
			this.startAlgorithm();
			System.out.println(this.summary(printPopulation));
			this.resetEnviroment();
		}
		
	}
	
	/**
	 * Inicjowanie generatora liczb pseudolosowych
	 */
	public static RandomGenerator getRandomGenerator() {
		if (rg == null) {
			rg = new Well44497b(new Date().getTime());
		}
		return rg;
	}
	
	
	public int getEnvNumberOfGenes() {
		return envNumberOfGenes;
	}

	public int getEnvLengthOfGene() {
		return envLengthOfGene;
	}

	public boolean[][] getForbiddenFields() {
		return this.forbiddenFields;
	}

	public int getMaxNumberOfQueens() {
		return this.MaxNumberOfQueens;
	}
	
	public void addSolution(String specimenString) {
		this.solutions.add(specimenString);
	}
	
	public String printSolutions(){
		String output = "";
		for (String sol : this.solutions) {
			output += sol;			
			output += "\n";
		}
		return output;
	}
}
