package net.zero323.meh_projekt_2;

/**
 * @author Maciej Szymkiewicz
 * Klasa reprezentująca pojedyńczego osobnika
 *
 */
public class Specimen implements Comparable<Specimen> {
	/**
	 * Chromosom 
	 */
	private Chromosome chromosome;
	/**
	 * Wiek osobnika
	 */
	private int age;
	/**
	 * Wartość funkcji przystosowania
	 */
	private double fitness;
	
	/**
	 * Konstruktor tworzący osobnika o losowym genomie o określonej długości
	 * @param numberOfGenes liczba genów (wierszy)
	 * @param lengthOfGene liczba kolumn
	 */
	public Specimen(final int numberOfGenes, final int lengthOfGene) {
		this.chromosome = new Chromosome(numberOfGenes, lengthOfGene);
		this.age = 0;
		this.fitness = this.chromosome.evaluateChromosome();
	}
	
	/**
	 * Konstuktor kopiujący
	 * @param other osobnik na podstawie którego tworzymy nowego osobnika 
	 */
	public Specimen(Specimen other) {
		this.chromosome = other.chromosome;
		this.age = 0;
		this.fitness = other.fitness;
	}
	
	/**
	 * Konstruktor tworzący osobnika z zadanym genomem 
	 * @param numberOfGenes liczba genów / wierszy
	 * @param lengthOfGene liczba kolumn
	 * @param chromosome chromosm tworzonego osobnika
	 */
	public Specimen(final int numberOfGenes, final int lengthOfGene, Chromosome chromosome) {
		this.chromosome = chromosome;
		this.age = 0;
		this.fitness = this.chromosome.evaluateChromosome();
	}	
	
	/**
	 * Metoda odpowiadajaca za krzyżowanie osobników.
	 * @param other drugi rodzic
	 * @return nowy osobnik bedący efektem krzyżowania jednopunktowego
	 */
	public Specimen crossbreed(final Specimen other) {
		Chromosome newChromosome = this.chromosome.crossover(other.chromosome);
		return  new Specimen(this.chromosome.getNumberOfGenes(), this.chromosome.getLengthOfGene(), newChromosome);
	}
	
	/**
	 * Metoda wywołująca mutację chromosomu danego osobnika (zamiana kolumny na inną)
	 * @param probability prawdopodobieństwo mutacji każdego z bitów genomu
	 * @return nowy osobnik o genomie będącym wynikiem mutacji genomu osobnika początkowego
	 */
	public Specimen mutate(double probability) {
		Chromosome newChromosome = this.chromosome.mutate(probability);
		return  new Specimen(this.chromosome.getNumberOfGenes(), this.chromosome.getLengthOfGene(), newChromosome);
	}
	
	/**
	 * Metoda wywołująca mutację chromosomu danego osobnika (zamiana koumn w wierszach)
	 * @param probability prawdopodobieństwo mutacji każdego z bitów genomu
	 * @return nowy osobnik o genomie będącym wynikiem mutacji genomu osobnika początkowego
	 */
	public Specimen swapMutate(double probabilityOfSwap) {
		Chromosome newChromosome = this.chromosome.swapMutate(probabilityOfSwap);
		return  new Specimen(this.chromosome.getNumberOfGenes(), this.chromosome.getLengthOfGene(), newChromosome);
	}
	
	
	/**
	 * Zwraca starszego o pokolenie osobnika będącego kopia aktualnego 
	 * @return
	 */
	public Specimen incrementAge() {
		Specimen specimen = new Specimen(this);
		specimen.age++;
		return specimen;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Specimen [fenotype: " + this.fenotypeToString() + ", fitness=" + fitness + "]";
	}
	
	/**
	 * Funkcja zwracająca napis będący reprezentacją fenotypu
	 * @return
	 */
	public String fenotypeToString(){
		String line = this.chromosome.toString();
		return line;
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Specimen other) {
		if (this.fitness - other.fitness < 1)
			return -1;
		else if (this.fitness - other.fitness > 1)
			return 1;
		else
			return 0;
	}


	/**
	 * Getter dla funkcji przystosowania
	 * @return wartość funkcji przystosowania dla danego osobnika
	 */
	public double getFitness() {
		return fitness;
	}
}
